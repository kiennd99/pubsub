package tool.rabbitmq.ReceiveMessageHandler.entity;

public class User {

    private String userName;
    private String fullName;
    private String password;
    private String email;
    private String organization;

    public User() {
    }

    public User(String userName, String fullName, String password, String email, String organization) {
        this.userName = userName;
        this.fullName = fullName;
        this.password = password;
        this.email = email;
        this.organization = organization;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }
}
