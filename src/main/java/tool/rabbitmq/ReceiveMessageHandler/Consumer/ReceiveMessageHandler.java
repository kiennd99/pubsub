package tool.rabbitmq.ReceiveMessageHandler.Consumer;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import tool.rabbitmq.ReceiveMessageHandler.entity.User;
import tool.rabbitmq.ReceiveMessageHandler.util.ConfigPram;
import tool.rabbitmq.ReceiveMessageHandler.util.PropsUtil;


import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Component
public class ReceiveMessageHandler {

    private static final Logger LOGGER = Logger.getLogger(ReceiveMessageHandler.class);
    private static final String USER_NAME_SSO= PropsUtil.get(ConfigPram.ssoUserName);
    private static final String PASSWORD_SSO=PropsUtil.get(ConfigPram.ssoPassword);
    private static final String URL_SSO=PropsUtil.get(ConfigPram.ssoUrlPost);

    public static void receiveMessage(String message)
    {
        LOGGER.info("Get User From Message>>>>>>");
        List<User> userList=ReceiveMessageHandler.getUser(message);
        LOGGER.info("Insert User To Sso");
        if(userList!=null)
        {
            for (User user: userList)
            {
                String strJson=ReceiveMessageHandler.createJson(user);
                try {
                    String result=postUser(USER_NAME_SSO,PASSWORD_SSO,URL_SSO,strJson);
                    LOGGER.info(result);
                } catch (Exception e) {
                    LOGGER.error(e);
                }
            }
        }
    }

    private static List<User> getUser(String message)
    {
        List<User> userList=null;
        JSONObject json=new JSONObject(message);
        if(json.getString("Type").toLowerCase().equals("user"))
        {
            if(json.getString("Action").toLowerCase().equals("insert"))
            {
                userList=new ArrayList<>();
                JSONArray data= json.getJSONArray("Data");
                for (int i=0; i<data.length(); i++)
                {
                    JSONObject object=data.getJSONObject(i);
                    User user=new User();
                    user.setUserName(object.getString("username"));
                    user.setPassword(object.getString("password"));
                    user.setEmail(object.getString("email"));;
                    user.setFullName(object.getString("fullname"));
                    user.setOrganization(object.getString("organization"));
                    userList.add(user);
                }
            }
        }
        return userList;
    }

    public static String getBase64Encode(String userName, String password) {
        LOGGER.info("Get Base64Encode ...");
        String str = userName + ":" + password;
        return Base64.getEncoder().encodeToString(str.getBytes());
    }

    public static String postUser(String userName, String password, String url, String json) throws IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {

        String strBase64 = ReceiveMessageHandler.getBase64Encode(userName, password);
        String result = "";
        SSLContextBuilder builder = new SSLContextBuilder();
        builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                builder.build());
        CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(
                sslsf).build();
        try {
            HttpPost httpPost = new HttpPost(url);
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Authorization", "Basic " + strBase64);
            httpPost.setHeader("Content-Type", "application/json");
            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(new KeyManager[0], new TrustManager[]{new ReceiveMessageHandler.DefaultTrustManager()}, new SecureRandom());
            SSLContext.setDefault(ctx);
            CloseableHttpResponse response = httpclient.execute(httpPost);
            try {

                HttpEntity httpEntity = response.getEntity();
                if (httpEntity != null) {
                    result = EntityUtils.toString(httpEntity);
                }
            } catch (Exception e) {
                LOGGER.error(e);
            } finally {
                response.close();
            }
        } catch (Exception e) {
            LOGGER.error(e);
        } finally {
            httpclient.close();
        }
        return result;
    }

    private static class DefaultTrustManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }

    public static String createJson(User user) {
        JSONObject json=new JSONObject();
        JSONObject name=new JSONObject();
        name.put("givenName",user.getFullName() );
        JSONArray email=new JSONArray();
        email.put(user.getEmail());
        JSONObject organization=new JSONObject();
        organization.put("organization", user.getOrganization());
        json.put("name",name);
        json.put("emails",email);
        json.put("userName",user.getUserName());
        json.put("password",user.getPassword());
        json.put("urn:ietf:params:scim:schemas:extension:enterprise:2.0:User",organization);
        return json.toString();
    }
}
