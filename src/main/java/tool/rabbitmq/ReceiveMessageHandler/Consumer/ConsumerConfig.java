package tool.rabbitmq.ReceiveMessageHandler.Consumer;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tool.rabbitmq.ReceiveMessageHandler.util.ConfigPram;
import tool.rabbitmq.ReceiveMessageHandler.util.PropsUtil;

@Configuration
public class ConsumerConfig {

    public static final String EXCHANGE_NAME= PropsUtil.get(ConfigPram.rabbitmqExchangeName);
    public static final String QUEUE_NAME=PropsUtil.get(ConfigPram.rabbitmqQueueName);
    public static final String ROUTING_KEY=PropsUtil.get(ConfigPram.rabbitmqRoutingKey);

    @Bean
    public TopicExchange topicExchange()
    {
        return new TopicExchange(EXCHANGE_NAME);
    }

    @Bean
    public Queue queue()
    {
        return new Queue(QUEUE_NAME, false, false, false);
    }

    @Bean
    public Binding binding()
    {
        return BindingBuilder.bind(queue()).to(topicExchange()).with(ROUTING_KEY);
    }

    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                             MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(QUEUE_NAME);
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean
    MessageListenerAdapter listenerAdapter(ReceiveMessageHandler receiver) {
        return new MessageListenerAdapter(receiver, "receiveMessage");
    }


}
