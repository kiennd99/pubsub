package tool.rabbitmq.ReceiveMessageHandler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReceiveMessageHandlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReceiveMessageHandlerApplication.class, args);
	}

}
